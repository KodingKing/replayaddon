package com.replaymod.hyperiumcompat.events.mixins;

import cc.hyperium.GuiStyle;
import cc.hyperium.config.Settings;
import cc.hyperium.gui.ChangeBackgroundGui;
import cc.hyperium.gui.HyperiumMainMenu;
import cc.hyperium.gui.ViewCosmeticsGui;
import cc.hyperium.gui.main.HyperiumMainGui;
import com.replaymod.replay.ReplayModReplay;
import com.replaymod.replay.gui.screen.GuiReplayViewer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiLanguage;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiMultiplayer;
import net.minecraft.client.gui.GuiOptions;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSelectWorld;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.realms.RealmsBridge;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.demo.DemoWorldServer;
import net.minecraft.world.storage.ISaveFormat;
import net.minecraft.world.storage.WorldInfo;
import org.lwjgl.input.Keyboard;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(HyperiumMainMenu.class)
public abstract class MixinHyperiumMainMenu extends GuiScreen {
    @Shadow private static boolean customBackground;

    @Shadow private static File customImage;

    @Shadow private BufferedImage bgBr;

    @Shadow private ResourceLocation bgDynamicTexture;

    @Shadow private DynamicTexture viewportTexture;

    @Shadow protected abstract void addSingleplayerMultiplayerButtons(int p_73969_1_, int p_73969_2_);

    @Shadow protected abstract GuiStyle getStyle();

    @Shadow protected abstract void addDefaultStyleOptionsButton(int j);

    @Shadow protected abstract void addHyperiumStyleOptionsButton(int j);

    @Shadow private GuiScreen field_183503_M;

    @Shadow private boolean field_183502_L;

    @Overwrite
    public void initGui() {
        customBackground = Settings.BACKGROUND.equalsIgnoreCase("CUSTOM");
        if (customImage.exists() && customBackground) {
            try {
                bgBr = ImageIO.read(new FileInputStream(customImage));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (bgBr != null)
                this.bgDynamicTexture = Minecraft.getMinecraft().getRenderManager().renderEngine.getDynamicTextureLocation(customImage.getName(), new DynamicTexture(bgBr));
            if (bgDynamicTexture == null)
                return;
        }

        this.viewportTexture = new DynamicTexture(256, 256);
        int j = this.height / 4 + 48;

        this.addSingleplayerMultiplayerButtons(j - 10, 24);
        this.buttonList.add(new GuiButton(100, 1, 1, "Cosmetic Shop"));
        this.buttonList.add(new GuiButton(76, (this.width / 2) - 100, 1, "Replay Viewer"));

        switch (this.getStyle()) {
            case DEFAULT:
                this.addDefaultStyleOptionsButton(j);
                break;
            case HYPERIUM:
                this.addHyperiumStyleOptionsButton(j);
                break;
        }

        this.mc.setConnectedToRealms(false);

        if (Minecraft.getMinecraft().gameSettings.getOptionOrdinalValue(GameSettings.Options.REALMS_NOTIFICATIONS) && !this.field_183502_L) {
            RealmsBridge realmsbridge = new RealmsBridge();
            this.field_183503_M = realmsbridge.getNotificationScreen(this);
            this.field_183502_L = true;
        }

        if (Minecraft.getMinecraft().gameSettings.getOptionOrdinalValue(GameSettings.Options.REALMS_NOTIFICATIONS) && this.field_183503_M != null) {
            this.field_183503_M.func_183500_a(this.width, this.height);
            this.field_183503_M.initGui();
        }
    }

    @Overwrite
    public void actionPerformed(GuiButton button) {

        if (button.id == 0) {
            this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
        }

        if (button.id == 5) {
            this.mc.displayGuiScreen(new GuiLanguage(this, this.mc.gameSettings, this.mc.getLanguageManager()));
        }

        if (button.id == 1) {
            this.mc.displayGuiScreen(new GuiSelectWorld(this));
        }

        if (button.id == 2) {
            this.mc.displayGuiScreen(new GuiMultiplayer(this));
        }

        if (button.id == 4) {
            this.mc.shutdown();
        }

        if (button.id == 11) {
            this.mc.launchIntegratedServer("Demo_World", "Demo_World", DemoWorldServer.demoWorldSettings);
        }

        if (button.id == 12) {
            ISaveFormat isaveformat = this.mc.getSaveLoader();
            WorldInfo worldinfo = isaveformat.getWorldInfo("Demo_World");

            if (worldinfo != null) {
                GuiYesNo guiyesno = GuiSelectWorld.func_152129_a(this, worldinfo.getWorldName(), 12);
                this.mc.displayGuiScreen(guiyesno);
            }
        }

        switch (getStyle()) {
            case DEFAULT:
                if (button.id == 15)
                    HyperiumMainGui.INSTANCE.show();
                if (button.id == 16)
                    Minecraft.getMinecraft().displayGuiScreen(new GuiConnecting(new GuiMainMenu(), Minecraft.getMinecraft(), Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) ? "stuck.hypixel.net" : "mc.hypixel.net", 25565));
                break;
            case HYPERIUM:
                if (button.id == 15)
                    HyperiumMainGui.INSTANCE.show();
                break;
        }

        if (button.id == 17)
            mc.displayGuiScreen(new ChangeBackgroundGui(this));
        if (button.id == 76)
            new GuiReplayViewer(ReplayModReplay.instance).display();
        if (button.id == 100) {
            mc.displayGuiScreen(new ViewCosmeticsGui());
        }
    }
}
