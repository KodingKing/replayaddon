package com.replaymod.hyperiumcompat.events.mixins;

import com.replaymod.hyperiumcompat.events.ClientDisconnectionFromServerEvent;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.S40PacketDisconnect;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(NetHandlerPlayClient.class)
public class MixinNetHandlerPlayClient {
    @Shadow @Final private NetworkManager netManager;

    /**
     * @reason events
     * @author amp
     */
    @Overwrite
    public void handleDisconnect(S40PacketDisconnect packetIn) {
        cc.hyperium.event.EventBus.INSTANCE.post(new ClientDisconnectionFromServerEvent());
        this.netManager.closeChannel(packetIn.getReason());
    }
}
