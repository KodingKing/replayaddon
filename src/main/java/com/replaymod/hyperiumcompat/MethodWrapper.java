package com.replaymod.hyperiumcompat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MethodWrapper {
    private Method inner;
    MethodWrapper(Method inner) {
        this.inner = inner;
    }
    public Object invoke(Object obj, Object... params) {
        try {
            return this.inner.invoke(obj, params);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }
}
