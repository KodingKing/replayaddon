package com.replaymod.core;

import cc.hyperium.Hyperium;
import cc.hyperium.event.EventBus;
import cc.hyperium.event.InitializationEvent;
import cc.hyperium.event.PreInitializationEvent;
import cc.hyperium.event.TickEvent;
import cc.hyperium.handlers.handlers.keybinds.HyperiumBind;
import cc.hyperium.internal.addons.IAddon;
import com.google.common.eventbus.Subscribe;
import com.google.common.io.Files;
import com.google.common.util.concurrent.ListenableFutureTask;
import com.replaymod.compat.ReplayModCompat;
import com.replaymod.config.Configuration;
import com.replaymod.core.gui.RestoreReplayGui;
import com.replaymod.core.utils.OpenGLUtils;
import com.replaymod.core.utils.ReplayEventBus;
import com.replaymod.editor.ReplayModEditor;
import com.replaymod.extras.ReplayModExtras;
import com.replaymod.optifinedetector.OptifineDetector;
import com.replaymod.recording.ReplayModRecording;
import com.replaymod.render.ReplayModRender;
import com.replaymod.render.utils.SoundHandler;
import com.replaymod.replay.ReplayModReplay;
import com.replaymod.replay.gui.screen.GuiReplayViewer;
import com.replaymod.replaystudio.util.I18n;
import com.replaymod.simplepathing.ReplayModSimplePathing;
import de.johni0702.minecraft.gui.container.GuiScreen;
import java.io.File;
import java.io.IOException;
import lombok.Getter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.io.FileUtils;

public class ReplayMod implements IAddon {

    @Getter(lazy = true)
    private static final String minecraftVersion = parseMinecraftVersion();

    private static String parseMinecraftVersion() {
        return "1.8.9";
    }


    public static final String MOD_ID = "replaymod";

    public static final ResourceLocation TEXTURE = new ResourceLocation("replaymod", "replay_gui.png");
    public static final int TEXTURE_SIZE = 256;
    OptifineDetector od = new OptifineDetector();
    private static final Minecraft mc = Minecraft.getMinecraft();

    public static Configuration config;
    @Deprecated
    public static SoundHandler soundHandler = new SoundHandler();

    private final KeyBindingRegistry keyBindingRegistry = new KeyBindingRegistry();
    private final SettingsRegistry settingsRegistry = new SettingsRegistry();

    // The instance of your mod that Forge uses.
    public static ReplayMod instance;


    public KeyBindingRegistry getKeyBindingRegistry() {
        return keyBindingRegistry;
    }

    public SettingsRegistry getSettingsRegistry() {
        return settingsRegistry;
    }

    public File getReplayFolder() throws IOException {
        String path = getSettingsRegistry().get(Setting.RECORDING_PATH);
        File folder = new File(path.startsWith("./") ? getMinecraft().mcDataDir : null, path);
        FileUtils.forceMkdir(folder);
        return folder;
    }

    @Subscribe
    public void preInit(PreInitializationEvent event) {
        // Initialize the static OpenGL info field from the minecraft main thread
        // Unfortunately lwjgl uses static methods so we have to make use of magic init calls as well


    }

    @Subscribe
    public void init(InitializationEvent event) {
        OpenGLUtils.init();

        I18n.setI18n(net.minecraft.client.resources.I18n::format);


        config = new Configuration(new File(Minecraft.getMinecraft().mcDataDir, "hyperium/replaymod.json"));
        config.load();
        settingsRegistry.setConfiguration(config);
        getSettingsRegistry().register(Setting.class);

        Hyperium.INSTANCE.getHandlers().getKeybindHandler().registerKeyBinding(new HyperiumBind("replaymod.input.settings", 0) {
            @Override
            public void onPress() {
                new GuiReplayViewer(ReplayModReplay.instance).display();
            }
        });

        settingsRegistry.save(); // Save default values to disk

        if(od.isOptifine())
            GameSettings.Options.RENDER_DISTANCE.setValueMax(64f);

        runLater(() -> {
            try {
                File[] files = getReplayFolder().listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file.isDirectory() && file.getName().endsWith(".mcpr.del")) {
                            if (file.lastModified() + 2 * 24 * 60 * 60 * 1000 < System.currentTimeMillis()) {
                                FileUtils.deleteDirectory(file);
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                File[] files = getReplayFolder().listFiles();
                if (files != null) {
                    for (File file : files) {
                        if (file.isDirectory() && file.getName().endsWith(".mcpr.tmp")) {
                            File origFile = new File(file.getParentFile(), Files.getNameWithoutExtension(file.getName()));
                            new RestoreReplayGui(GuiScreen.wrap(mc.currentScreen), origFile).display();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Set when the currently running code has been scheduled by runLater.
     * If this is the case, subsequent calls to runLater have to be delayed until all scheduled tasks have been
     * processed, otherwise a livelock may occur.
     */
    private boolean inRunLater = false;


    public void runLater(Runnable runnable) {
        if (mc.isCallingFromMinecraftThread() && inRunLater) {
            EventBus bus = EventBus.INSTANCE;
            bus.register(new Object() {
                @Subscribe
                public void onTick(TickEvent event) {
                        runLater(runnable);
                        bus.unregister(this);
                }
            });
            return;
        }
        mc.addScheduledTask((ListenableFutureTask.create(() -> {
            inRunLater = true;
            try {
                runnable.run();
            } finally {
                inRunLater = false;
            }
        }, null)));
    }


    public String getVersion() {
        return "1.0";
    }

    public Minecraft getMinecraft() {
        return mc;
    }

    public void printInfoToChat(String message, Object... args) {
        printToChat(false, message, args);
    }

    public void printWarningToChat(String message, Object... args) {
        printToChat(true, message, args);
    }

    private void printToChat(boolean warning, String message, Object... args) {
        if (getSettingsRegistry().get(Setting.NOTIFICATIONS)) {
            // Some nostalgia: "§8[§6Replay Mod§8]§r Your message goes here"
            ChatStyle coloredDarkGray = new ChatStyle().setColor(EnumChatFormatting.DARK_GRAY);
            ChatStyle coloredGold = new ChatStyle().setColor(EnumChatFormatting.GOLD);
            IChatComponent text = new ChatComponentText("[").setChatStyle(coloredDarkGray)
                    .appendSibling(new ChatComponentTranslation("replaymod.title").setChatStyle(coloredGold))
                    .appendSibling(new ChatComponentText("] "))
                    .appendSibling(new ChatComponentTranslation(message, args).setChatStyle(new ChatStyle()
                            .setColor(warning ? EnumChatFormatting.RED : EnumChatFormatting.DARK_GREEN)));
            // Send message to chat GUI
            // The ingame GUI is initialized at startup, therefore this is possible before the client is connected
            mc.ingameGUI.getChatGUI().printChatMessage(text);
        }
    }

    @Override
    public void onLoad() {
        instance = this;
        ReplayEventBus.INSTANCE.register(new ReplayModSimplePathing());
        ReplayEventBus.INSTANCE.register(new ReplayModSimplePathing());
        ReplayEventBus.INSTANCE.register(new ReplayModCompat());
        ReplayEventBus.INSTANCE.register(new ReplayModRecording());
        ReplayEventBus.INSTANCE.register(new ReplayModReplay());
        ReplayEventBus.INSTANCE.register(new ReplayModEditor());
        ReplayEventBus.INSTANCE.register(new ReplayModRender());
        ReplayEventBus.INSTANCE.register(new ReplayModExtras());
        ReplayEventBus.PRIORITY_HIGH.register(this);
    }

    @Override
    public void onClose() {

    }

    @Override
    public void sendDebugInfo() {

    }
}
