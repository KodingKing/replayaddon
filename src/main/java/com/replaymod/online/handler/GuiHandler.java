package com.replaymod.online.handler;

import com.replaymod.core.utils.ReplayEventBus;
import com.replaymod.online.ReplayModOnline;

public class GuiHandler {
    private static final int BUTTON_REPLAY_CENTER = 17890236;

    private final ReplayModOnline mod;

    public GuiHandler(ReplayModOnline mod) {
        this.mod = mod;
    }

    public void register() {
        ReplayEventBus.INSTANCE.register(this);
    }
}
